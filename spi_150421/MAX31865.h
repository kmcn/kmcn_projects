
/** @file
* @brief MAX31865 Library
* @defgroup 
* @author kmcn
*/

#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "nordic_common.h"
#include "boards.h"
#include "nrf_delay.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include <string.h>

#ifndef MAX31865_H_
#define MAX31865_H_

/* Private variables ---------------------------------------------------------*/
#define MAX31865_CONFIG_REG       0x00
#define MAX31865_CONFIG_BIAS      0x80
#define MAX31865_CONFIG_MODEAUTO  0x40
#define MAX31865_CONFIG_MODEOFF   0x00
#define MAX31865_CONFIG_1SHOT     0x20
#define MAX31865_CONFIG_3WIRE     0x10
#define MAX31865_CONFIG_24WIRE    0x00
#define MAX31865_CONFIG_FAULTSTAT 0x02
#define MAX31865_CONFIG_FILT50HZ  0x01
#define MAX31865_CONFIG_FILT60HZ  0x00

#define MAX31865_RTDMSB_REG       0x01
#define MAX31865_RTDLSB_REG       0x02
#define MAX31865_HFAULTMSB_REG    0x03
#define MAX31865_HFAULTLSB_REG    0x04
#define MAX31865_LFAULTMSB_REG    0x05
#define MAX31865_LFAULTLSB_REG    0x06
#define MAX31865_FAULTSTAT_REG    0x07

#define MAX31865_FAULT_HIGHTHRESH 0x80
#define MAX31865_FAULT_LOWTHRESH  0x40
#define MAX31865_FAULT_REFINLOW   0x20
#define MAX31865_FAULT_REFINHIGH  0x10
#define MAX31865_FAULT_RTDINLOW   0x08
#define MAX31865_FAULT_OVUV       0x04

#define RTD_A 3.9083e-3
#define RTD_B -5.775e-7

#define RTD_NOMINAL 100
#define RTD_REF     430

/* Function Declarations -------------------------------------------------------*/
void spi_event_handler(nrf_drv_spi_evt_t const *p_event, void *p_context);
void max31865_init(void);
void max31865_deinit(void);
void max31865_read_temp(void);
void max31865_spi_begin(void);
void max31865_temperature(void);


uint8_t readFault(void);
void clearFault(void);
void enableBias(bool b);
void autoConvert(bool b);
void enable50Hz(bool b);
void setWires(void);
float temperature(float RTDnominal, float refResistor);
uint16_t readRTD(void);
uint8_t readRegister8(uint8_t addr);
uint16_t readRegister16(uint8_t addr);
void readRegisterN(uint8_t addr, uint8_t buffer[], uint8_t n);
void writeRegister8(uint8_t addr, uint8_t data);

#endif