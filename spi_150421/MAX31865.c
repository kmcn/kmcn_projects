/**
***************************************************************
* @file MAX31865.c
* @author Kate McNamara 
* @date 2021-04-15
* @brief MAX31865 temperature sensor peripheral driver
***************************************************************
* EXTERNAL FUNCTIONS
***************************************************************
***************************************************************
*/


/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "nordic_common.h"
#include "boards.h"
#include "nrf_delay.h"



#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include "MAX31865.h"


/* Defines -----------------------------------------------------------*/
#define SPI_INSTANCE  0 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

#define TEST_STRING     "Nordic"
static uint8_t       m_tx_buf[] = TEST_STRING;           /**< TX buffer. */
static uint8_t       m_rx_buf[sizeof(TEST_STRING) + 1];    /**< RX buffer. */
static const uint8_t m_length = sizeof(m_tx_buf);        /**< Transfer length. */


/**
  * @brief  Initialise SPI pins
  * @param  None
  * @retval None
  */
void max31865_init(void) {

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = SPI_SS_PIN;
    spi_config.miso_pin = SPI_MISO_PIN;
    spi_config.mosi_pin = SPI_MOSI_PIN;
    spi_config.sck_pin  = SPI_SCK_PIN;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));

    NRF_LOG_INFO("SPI example started.");

}

/**
  * @brief  
  * @param  None
  * @retval None
  */
void max31865_deinit(void) {

  nrf_drv_spi_uninit(&spi);

}

/**
  * @brief  
  * @param  None
  * @retval None
  */
void max31865_temperature(void) {

    float temp = temperature(RTD_NOMINAL, RTD_REF);
    NRF_LOG_INFO("%f", temp);
}


/**
  * @brief  Begin SPI
  * @param  None
  * @retval None
  */
void max31865_spi_begin(void) {

//memset(m_rx_buf, 0, m_length);
//spi_xfer_done = false;
//APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf, m_length, m_rx_buf, m_length));
        //NRF_LOG_INFO("X");

	enableBias(false);
	autoConvert(false);
	clearFault();
}

/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context) {
    spi_xfer_done = true;
    NRF_LOG_INFO("Transfer completed.");
}

/*****************************************************************************/
/**************************************************************************/
/*!
    @brief Read the raw 8-bit FAULTSTAT register
    @return The raw unsigned 8-bit FAULT status register
*/
/**************************************************************************/
uint8_t readFault(void) {
	return readRegister8(MAX31865_FAULTSTAT_REG);
}

/**************************************************************************/
/*!
    @brief Clear all faults in FAULTSTAT
*/
/**************************************************************************/
void clearFault(void) {
	uint8_t t = readRegister8(MAX31865_CONFIG_REG);
	t &= ~0x2C;
	t |= MAX31865_CONFIG_FAULTSTAT;
	writeRegister8(MAX31865_CONFIG_REG, t);
}

/**************************************************************************/
/*!
    @brief Enable the bias voltage on the RTD sensor
    @param b If true bias is enabled, else disabled
*/
/**************************************************************************/
void enableBias(bool b) {
        
	uint8_t t = readRegister8(MAX31865_CONFIG_REG);
	if (b) {
		t |= MAX31865_CONFIG_BIAS; // enable bias
	} else {
		t &= ~MAX31865_CONFIG_BIAS; // disable bias
	}

        nrf_delay_ms(20);
	writeRegister8(MAX31865_CONFIG_REG, t);
}

/**************************************************************************/
/*!
    @brief Whether we want to have continuous conversions (50/60 Hz)
    @param b If true, auto conversion is enabled
*/
/**************************************************************************/
void autoConvert(bool b) {
	uint8_t t = readRegister8(MAX31865_CONFIG_REG);
	if (b) {
		t |= MAX31865_CONFIG_MODEAUTO; // enable autoconvert
	} else {
		t &= ~MAX31865_CONFIG_MODEAUTO; // disable autoconvert
	}
	writeRegister8(MAX31865_CONFIG_REG, t);
}

/**************************************************************************/
/*!
    @brief Whether we want filter out 50Hz noise or 60Hz noise
    @param b If true, 50Hz noise is filtered, else 60Hz(default)
*/
/**************************************************************************/

void enable50Hz(bool b) {
	uint8_t t = readRegister8(MAX31865_CONFIG_REG);
	if (b) {
		t |= MAX31865_CONFIG_FILT50HZ;
	} else {
		t &= ~MAX31865_CONFIG_FILT50HZ;
	}
	writeRegister8(MAX31865_CONFIG_REG, t);
}

/**************************************************************************/
/*!
    @brief How many wires we have in our RTD setup, can be MAX31865_2WIRE,
    MAX31865_3WIRE, or MAX31865_4WIRE
    @param wires The number of wires in enum format
*/
/**************************************************************************/
void setWires(void) {
	uint8_t t = readRegister8(MAX31865_CONFIG_REG);
                  //if (wires == MAX31865_3WIRE) {
                  //  t |= MAX31865_CONFIG_3WIRE;
                  //} else {
                  // 2 or 4 wire
	t &= ~MAX31865_CONFIG_3WIRE;
                    //}
	writeRegister8(MAX31865_CONFIG_REG, t);
}

/**************************************************************************/
/*!
    @brief Read the temperature in C from the RTD through calculation of the
    resistance. Uses
   http://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf
   technique
    @param RTDnominal The 'nominal' resistance of the RTD sensor, usually 100
    or 1000
    @param refResistor The value of the matching reference resistor, usually
    430 or 4300
    @returns Temperature in C
*/
/**************************************************************************/
float temperature(float RTDnominal, float refResistor) {
  float Z1, Z2, Z3, Z4, Rt, temp;

  Rt = readRTD();
  Rt /= 32768;
  Rt *= refResistor;

  // Serial.print("\nResistance: "); Serial.println(Rt, 8);

  Z1 = -RTD_A;
  Z2 = RTD_A * RTD_A - (4 * RTD_B);
  Z3 = (4 * RTD_B) / RTDnominal;
  Z4 = 2 * RTD_B;

  temp = Z2 + (Z3 * Rt);
  temp = (sqrt(temp) + Z1) / Z4;

  if (temp >= 0)
    return temp;

  // ugh.
  Rt /= RTDnominal;
  Rt *= 100; // normalize to 100 ohm

  float rpoly = Rt;

  temp = -242.02;
  temp += 2.2228 * rpoly;
  rpoly *= Rt; // square
  temp += 2.5859e-3 * rpoly;
  rpoly *= Rt; // ^3
  temp -= 4.8260e-6 * rpoly;
  rpoly *= Rt; // ^4
  temp -= 2.8183e-8 * rpoly;
  rpoly *= Rt; // ^5
  temp += 1.5243e-10 * rpoly;

  return temp;
}

/**************************************************************************/
/*!
    @brief Read the raw 16-bit value from the RTD_REG in one shot mode
    @return The raw unsigned 16-bit value, NOT temperature!
*/
/**************************************************************************/
uint16_t readRTD(void) {
	clearFault();
	enableBias(true);
	
        nrf_delay_ms(10);
	
        uint8_t t = readRegister8(MAX31865_CONFIG_REG);
	t |= MAX31865_CONFIG_1SHOT;
	writeRegister8(MAX31865_CONFIG_REG, t);
	
        nrf_delay_ms(65);

	uint16_t rtd = readRegister16(MAX31865_RTDMSB_REG);

	enableBias(false); // Disable bias current again to reduce selfheating.

	// remove fault
	rtd >>= 1;

        NRF_LOG_INFO("RTD:%d", rtd);
	return rtd;
}

/**********************************************/

uint8_t readRegister8(uint8_t addr) {
	uint8_t ret = 0;
	readRegisterN(addr, &ret, 1);

	return ret;
}

uint16_t readRegister16(uint8_t addr) {
	uint8_t buffer[2] = {0, 0};
	readRegisterN(addr, buffer, 2);

	uint16_t ret = buffer[0];
	ret <<= 8;
	ret |= buffer[1];

	return ret;
}

void readRegisterN(uint8_t addr, uint8_t buffer[], uint8_t n) {
	addr &= 0x7F; // make sure top bit is not set
	//spi_dev.write_then_read(&addr, 1, buffer, n);
        NRF_LOG_INFO("W:%d", addr);
	
        APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, &addr, 1, buffer, 1));
       
        NRF_LOG_INFO("R:%d", buffer);
}

void writeRegister8(uint8_t addr, uint8_t data) {
	addr |= 0x80; // make sure top bit is set
	uint8_t buffer[2] = {addr, data};
        
        NRF_LOG_INFO("W:%d, %d", buffer[0], buffer[1]);
	//spi_dev.write(buffer, 2);  /// CHANGE 
	
        APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, buffer, sizeof(buffer), NULL, NULL));
}