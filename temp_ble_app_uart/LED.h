/**
***************************************************************
* @file LED.h
* @author Kate McNamara 
* @date 2021-05-25
* @brief RGB LED peripheral driver header file
***************************************************************
* EXTERNAL FUNCTIONS
***************************************************************
***************************************************************
*/

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nrf.h"
#include "nordic_common.h"
#include "boards.h"
#include "nrf_delay.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "app_error.h"


#ifndef LED_H_
#define LED_H_

#define R_LED_PIN 6  // P0.06
#define G_LED_PIN 41 // P1.09
#define B_LED_PIN 8  // P0.08

void led_init(void);
void led_deinit(void);
void led_toggle(uint8_t ledPin);
void led_set(uint8_t ledPin);
void led_clear(uint8_t ledPin);

#endif