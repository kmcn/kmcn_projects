/**
***************************************************************
* @file   MAX31865.c
* @author Kate McNamara 
* @date   2021-04-15
* @brief  MAX31865 temperature sensor peripheral driver for nRF52840 - source file
* @about  Adapted from https://github.com/nimaltd/max31865 and https://github.com/adafruit/Adafruit_MAX31865, 
          licensed under GNU General Public License V3.0
***************************************************************
*/

/* Includes -----------------------------------------------------------------*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* Nordic Includes ----------------------------------------------------------*/
#include "app_error.h"
#include "app_util_platform.h"
#include "boards.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* Private Includes ---------------------------------------------------------*/
#include "max31865.h"

/* Private Defines ----------------------------------------------------------*/
#define PIN_LOW   		0 
#define PIN_HIGH  		1
#define SPI_INSTANCE 	0 /** SPI instance index. */

/* Global Variables  --------------------------------------------------------*/
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static const nrf_drv_spi_t spi_handle = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */

/* Functions ----------------------------------------------------------------*/

/**
  * @brief Read operation of data transferred out of SDO (MOSI). 
  		If n = 1, only a single read occurs. 
		If n > 1, n reads will occur.
		Received data stored in buffer variable.
  * @param
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t addr - address to specify write
		uint8_t *buffer - point to uin8_t to store data received from SDO. Received value returns to buffer upon function call.
		uint8_t n - number of read operations
  * @retval
		None
  */
void read_register_n(Max31865 *max31865, uint8_t addr, uint8_t *buffer, uint8_t n) {

	uint8_t tmp = 0xFF;
	addr &= 0x7F; 			// Make sure top bit is not set

	nrf_gpio_pin_write(max31865->cs_pin, PIN_LOW); 								// Set CS pin low
	nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); 								// Set MOSI pin high          
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_handle, &addr, 1, NULL, 0)); 		// Write ONLY operation to set address
	nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); 								// Set MOSI pin high

	while (n--) { 																// n reads will occur               
			
		nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); 							// Set MOSI pin high
		APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_handle, NULL, 0, buffer, 1)); // Read ONLY operation to read data from SDO
		nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); 							// Set MOSI pin high

		buffer++;
	}
		
	nrf_gpio_pin_write(max31865->cs_pin, PIN_HIGH); 	// Set CS pin high
}

/**
  * @brief Read operation of 8-bit data transferred out of SDO (MOSI). 
		Transfer requires address of the byte to specify read, followed by one bytes of data.
		Received data stored in ret variable.
  * @param  
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t addr - address to specify write
  * @retval 
		uint8_t ret - integer value of register data
  */
uint8_t read_register_8(Max31865 *max31865, uint8_t addr) {
        
	uint8_t ret = 0;
	read_register_n(max31865, addr, &ret, 1);

	return ret;  
}
/**
  * @brief  Read operation of 16-bit data transferred out of SDO (MOSI). 
  		Transfer requires address of the byte to specify read, followed by two bytes of data.
		Received data stored in ret variable.
  * @param  
  		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t addr - address to specify write
  * @retval 
  		uint16_t ret - integer value of register data
  */
uint16_t read_register_16(Max31865 *max31865, uint8_t addr) {
	
	uint8_t buffer[2] = {0, 0};						// 2 x 8-bit register values
	read_register_n(max31865, addr, buffer, 2);
	uint16_t ret = buffer[0];
	ret <<= 8;
	ret |=  buffer[1];

	return ret;
}

/**
  * @brief  Write operation of 8-bit data transferred to SDI (MISO). 
		Transfer requires address of the byte to specify write, followed by one byte of data.
  * @param  
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t addr - address to specify write
		uint8_t data - data to be written
  * @retval 
		None
  */
void write_register_8(Max31865 *max31865, uint8_t addr, uint8_t data) {

	nrf_gpio_pin_write(max31865->cs_pin, PIN_LOW); // Set CS pin low
	addr |= 0x80;

	nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); // Set MOSI pin high
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_handle, &addr, 1, NULL, 0));
		
	nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); // Set MOSI pin high
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi_handle, &data, 1, NULL, 0));
		
	nrf_gpio_pin_write(SPI_MOSI_PIN, PIN_HIGH); // Set MOSI pin high
	nrf_gpio_pin_write(max31865->cs_pin, PIN_HIGH); // Set CS pin high
}

/**
  * @brief  The fault detection cycle checks for three faults by making the following voltage 
  		comparisons and setting the associated bits in the Fault Status Register:
		1) Is the voltage at REFIN- greater than 85% x VBIAS? 
			(Fault Status Register bit D5)
		2) Is the voltage at REFIN- less than 85% x VBIAS when FORCE- input switch is open? 
			(Fault Status Register bit D4)
		3) Is the voltage at RTDIN- less than 85% x VBIAS when FORCE- input switch is open? 
			(Fault Status Register bit D3)
		
		The High/Low Fault Threshold registers select the trip thresholds for RTD fault detection. 
			(Fault Status Register bit D[7:6])
				High Fault Threshold - 0xFFFF
				Low Fault Threshold - 0x0000
  * @param 
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
  * @retval 
		uint8_t fault value
  */
uint8_t read_fault(Max31865 *max31865) {
 
    return read_register_8(max31865, MAX31865_FAULTSTAT_REG);
}

/**
  * @brief  Write a 1 to this bit while writing 0 to bits D5, D3, and D2 to return all fault status
  		bits (D[7:2]) in the Fault Status Register to 0. Note that bit D2 in the Fault Register, and
		subsequently bit D0 in the RTD LSB register may be set again immediately after resetting if 
		an over/undervoltage fault persists. The fault status clear bit D1, self-clears to 0.
  * @param  
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
  * @retval 
  		None
  */
void clear_fault(Max31865 *max31865) {

	uint8_t t = read_register_8(max31865, MAX31865_CONFIG_REG);
	t &= ~0x2C;
	t |= MAX31865_CONFIG_FAULTSTAT;
	write_register_8(max31865, MAX31865_CONFIG_REG, t);
}

/**
  * @brief  When no conversions are being performed, VBIAS may be disabled to reduce power dissipation. 
  		Write 1 to this bit to enable VBIAS before beginning a single (1-Shot) conversion. When 
		automatic (continuous) conversion mode is selected, VBIAS remains on continuously.
  * @param
  		Max31865 *max31865 - pointer to struct containing CS pin and lock flag. 
		uint8_t enable - integer value: 1 to enable, 0 to disable
  * @retval 
		None
  */
void enable_bias(Max31865 *max31865, uint8_t enable) {
	
	uint8_t t = read_register_8(max31865, MAX31865_CONFIG_REG);

	if (enable) {
		t |= MAX31865_CONFIG_BIAS;
	} else {
		t &= ~MAX31865_CONFIG_BIAS;
	}

	write_register_8(max31865, MAX31865_CONFIG_REG, t);
}

/**
  * @brief  Write 1 to this bit to select automatic conversion mode, in which conversions occur
  		continuously at a 50/60Hz rate. Write 0 to this bit to exit automatic conversion mode and
		enter the "Normally Off" mode. 1-shot conversions may be initiated from this mode.
  * @param  
  		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t enable - integer value: 1 to enable, 0 to disable
  * @retval 
		None
  */
void auto_convert(Max31865 *max31865, uint8_t enable) {
	
	uint8_t t = read_register_8(max31865, MAX31865_CONFIG_REG);

	if (enable) {
		t |= MAX31865_CONFIG_MODEAUTO;
    } else {
		t &= ~MAX31865_CONFIG_MODEAUTO; 
    }
	
	write_register_8(max31865, MAX31865_CONFIG_REG, t);
}

/**
  * @brief  The configuration register selects RTD connection (either 3-wire or 2-wire/4-wire)
  * @param  
  		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t numWires - integer number for number of wires for sensor connection: 2, 3 or 4 wires
  * @retval 
		None
  */
void set_wires(Max31865 *max31865, uint8_t numWires) {
	
	uint8_t t = read_register_8(max31865, MAX31865_CONFIG_REG);

	if (numWires == 3) {
		t |= MAX31865_CONFIG_3WIRE;
	} else {
		t &= ~MAX31865_CONFIG_3WIRE;
    }
	
	write_register_8(max31865, MAX31865_CONFIG_REG, t);
}

/**
  * @brief This bit selects the notch frequencies for the noise
		rejection filter. Write 0 to this bit to reject 60Hz and
		its harmonics; write 1 to this bit to reject 50Hz and its
		harmonics. Note: Do not change the notch frequency
		while in auto conversion mode.
  * @param  
  		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t filterHz - integer number for digital sinc filter frequency: 50 or 60Hz
  * @retval 
  */
void set_filter(Max31865 *max31865, uint8_t filterHz) {
	
	uint8_t t = read_register_8(max31865, MAX31865_CONFIG_REG);

	if (filterHz == 50) {
		t |= MAX31865_CONFIG_FILT50HZ;
    } else {
		t &= ~MAX31865_CONFIG_FILT50HZ;
    }
	
	write_register_8(max31865, MAX31865_CONFIG_REG, t);
}

/**
  * @brief Two 8-bit registers, RTD MSBs and RTD LSBs, contain the RTD resistance data. 
  		D0 of the RTD LSBs register is a Fault bit that indicates whether any RTD faults 
		have been detected.
  * @param  
  		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
  * @retval 
		uint16_t rtd - 15-bit ratio of RTD resistance to reference resistance. 
  */
uint16_t read_rtd(Max31865 *max31865) {

	clear_fault(max31865);
	enable_bias(max31865, 1); 			// Enable bias

	nrf_delay_ms(10);
	uint8_t t = read_register_8(max31865, MAX31865_CONFIG_REG);

	t |= MAX31865_CONFIG_1SHOT;
	write_register_8(max31865, MAX31865_CONFIG_REG, t);

    nrf_delay_ms(65); 					// A single conversion requires approximately 62.5ms in 50Hz filter mode to complete

	uint16_t rtd = read_register_16(max31865, MAX31865_RTDMSB_REG);
        
    enable_bias(max31865, 0); 			// Disable bias

	rtd >>= 1; 							// Remove D0 (Fault bit)

	return rtd;
}

/**
  * @brief Initialise GPIO pins for SPI, configure SPI driver, initialise MAX31865 set up. 
  * @param  
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		uint8_t numWires - integer number for number of wires for sensor connection: 2, 3 or 4 wires
		uint8_t filterHz - integer number for digital sinc filter frequency: 50 or 60Hz
  * @retval None
  */
void max318565_init(Max31865 *max31865, uint8_t numWires, uint8_t filterHz) {
	
	if (max31865->lock == 1) { 
		nrf_delay_ms(1);
	}
  
	nrf_gpio_cfg_output(SPI_SS_PIN); 						// Set CS as output
	nrf_gpio_cfg_output(SPI_MOSI_PIN); 						// Set MOSI as output
	nrf_gpio_cfg_output(SPI_SCK_PIN); 						// Set SCK as output
	nrf_gpio_cfg_input(SPI_MISO_PIN, NRF_GPIO_PIN_PULLUP); 	// Set MISO as input

	nrf_drv_spi_config_t spi_config;  
	spi_config.orc = 0xFF;
	spi_config.ss_pin = NRF_DRV_SPI_PIN_NOT_USED;			// CS pin set high/low manually using max31865 struct
	spi_config.miso_pin = SPI_MISO_PIN;						// Set MISO pin
	spi_config.mosi_pin = SPI_MOSI_PIN;						// Set MOSI pin
	spi_config.sck_pin  = SPI_SCK_PIN;						// Set SCK pin
	spi_config.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST; // Most significant bit shifted out first.
	spi_config.frequency = NRF_DRV_SPI_FREQ_2M; 			// 2 Mbps
	spi_config.mode = NRF_DRV_SPI_MODE_1; 					// SCK active high, sample on trailing edge of clock.

	APP_ERROR_CHECK(nrf_drv_spi_init(&spi_handle, &spi_config, NULL, NULL)); 	// Initialise SPI driver

	max31865->lock = 1;
	max31865->cs_pin = SPI_SS_PIN; 							// Set CS pin

	nrf_gpio_pin_write(max31865->cs_pin, PIN_HIGH);			// Set CS pin high
	nrf_delay_ms(100);

	set_wires(max31865, numWires);							// Set number of wires
	enable_bias(max31865, 0);								// Disable Bias Voltage to reduce power dissipation
	auto_convert(max31865, 0);								// Disable automatic conversion mode to enter "Normally Off" mode
	clear_fault(max31865);
	set_filter(max31865, filterHz);  						// Set filter value to reject harmonics
}

/**
  * @brief Function to retrieve value from MAX31865 RTD Data Register and convert to  temperature (Celsius).
  * @param
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		float *readTemp - pointer to float for converted temperature value. 
			Temperature value (Celsius) returned to float upon function call.
  * @retval bool isOk - boolean value, true if value retrieval successfull, otherwise false.
  */
bool read_temperature(Max31865 *max31865, float *readTemp) {

    if (max31865->lock == 1) {
    	nrf_delay_ms(1);
    }

    max31865->lock = 1;
    bool isOk = false;
    float Z1, Z2, Z3, Z4, Rt, temp;
    Rt = read_rtd(max31865);
    Rt /= 32768;
    Rt *= _MAX31865_RREF;
    Z1 = -RTD_A;
    Z2 = RTD_A * RTD_A - (4 * RTD_B);
    Z3 = (4 * RTD_B) / _MAX31865_RNOMINAL;
    Z4 = 2 * RTD_B;

    temp = Z2 + (Z3 * Rt);
    temp = (sqrtf(temp) + Z1) / Z4;
    
    NRF_LOG_ERROR("Temperature " NRF_LOG_FLOAT_MARKER "\r\n", NRF_LOG_FLOAT(temp));

    if (temp >= 0) {

    	*readTemp = temp; 
      
    	if (read_fault(max31865) == 0) {
        	isOk = true;        
    	}

      	max31865->lock = 0;
      	return isOk;

    } else {

		Rt /= _MAX31865_RNOMINAL;
		Rt *= 100;    
		float rpoly = Rt;
		temp = -242.02;
		temp += 2.2228 * rpoly;
		rpoly *= Rt;  // square
		temp += 2.5859e-3 * rpoly;
		rpoly *= Rt;  // ^3
		temp -= 4.8260e-6 * rpoly;
		rpoly *= Rt;  // ^4
		temp -= 2.8183e-8 * rpoly;
		rpoly *= Rt;  // ^5
		temp += 1.5243e-10 * rpoly;

		*readTemp = temp; 

		uint8_t fault = read_fault(max31865); // check for faults from MAX31865
      
	  	if (!fault) {
        	isOk = true;
      	} else {

			if (fault & MAX31865_FAULT_HIGHTHRESH) {
				NRF_LOG_INFO("RTD High Threshold"); 
			}

			if (fault & MAX31865_FAULT_LOWTHRESH) {
				NRF_LOG_INFO("RTD Low Threshold"); 
			}

			if (fault & MAX31865_FAULT_REFINLOW) {
				NRF_LOG_INFO("REFIN- > 0.85 x Bias"); 
			}

			if (fault & MAX31865_FAULT_REFINHIGH) {
				NRF_LOG_INFO("REFIN- < 0.85 x Bias - FORCE- open"); 
			}

			if (fault & MAX31865_FAULT_RTDINLOW) {
				NRF_LOG_INFO("RTDIN- < 0.85 x Bias - FORCE- open"); 
			}

			if (fault & MAX31865_FAULT_OVUV) {
				NRF_LOG_INFO("Under/Over voltage"); 
			}
      	}

		clear_fault(max31865);

		max31865->lock = 0;
		return isOk;  
    }
}

/**
  * @brief Converts temperature retrieved from read_temperature() from Celsius to Fahrenheit.
  * @param
		Max31865 *max31865 - pointer to struct containing CS pin and lock flag.
		float *readTemp - pointer to float for converted temperature value. 
			Temperature value (Fahrenheit) returns to float on function call.
  * @retval bool isOk - boolean value, true if value retrieval successfull, otherwise false. 
  */
bool  read_temperature_fahrenheit(Max31865 *max31865, float *readTemp) {  

	bool isOk = read_temperature(max31865, readTemp);
	*readTemp = (*readTemp * 9.0f / 5.0f) + 32.0f;
	return isOk;
}

/**
  * @brief  
  * @param  
  * @retval 
  */
float filter_temperature(float newInput, float lastOutput, float efectiveFactor) {

	return ((float)lastOutput * (1.0f - efectiveFactor)) + ((float)newInput * efectiveFactor);
}


