/**
***************************************************************
* @file LED.c
* @author Kate McNamara 
* @date 2021-05-25
* @brief RGB LED peripheral driver source file
***************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "app_error.h"
#include "app_util_platform.h"
#include "boards.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* Private Includes ---------------------------------------------------------*/
#include "led.h"

/* Functions ----------------------------------------------------------------*/

/**
  * @brief Initialise pins for RGB LED as outputs.
  * @param
        None
  * @retval
		None
  */
void led_init(void) {

    nrf_gpio_cfg_output(R_LED_PIN);
    nrf_gpio_cfg_output(G_LED_PIN);
    nrf_gpio_cfg_output(B_LED_PIN);
}

/**
  * @brief Deinitialise pins for RGB LED.
  * @param
        None
  * @retval
		None
  */
void led_deinit(void) {

    nrf_gpio_cfg_default(R_LED_PIN);
    nrf_gpio_cfg_default(G_LED_PIN);
    nrf_gpio_cfg_default(B_LED_PIN);
}

/**
  * @brief Toggle RGB Pins
  * @param
        None
  * @retval
		None
  */
void led_toggle(uint8_t ledPin) {

    switch (ledPin) {

        case (R_LED_PIN):
            nrf_gpio_pin_toggle(R_LED_PIN);
            break;

        case (G_LED_PIN):
            nrf_gpio_pin_toggle(G_LED_PIN);
            break;

        case (B_LED_PIN):
            nrf_gpio_pin_toggle(B_LED_PIN);
            break;

        default:
            break;
    }
}

/**
  * @brief Set RGB Pin High
  * @param
        None
  * @retval
		None
  */
void led_set(uint8_t ledPin) {

  switch (ledPin) {

        case (R_LED_PIN):
            nrf_gpio_pin_write(R_LED_PIN, 1);
            break;

        case (G_LED_PIN):
            nrf_gpio_pin_write(G_LED_PIN, 1);
            break;

        case (B_LED_PIN):
            nrf_gpio_pin_write(B_LED_PIN, 1);
            break;
            
        default:
            break;
    }

}

/**
  * @brief Clear RGB Pin
  * @param
        None
  * @retval
		None
  */
void led_clear(uint8_t ledPin) {

  switch (ledPin) {

        case (R_LED_PIN):
            nrf_gpio_pin_write(R_LED_PIN, 0);
            break;

        case (G_LED_PIN):
            nrf_gpio_pin_write(G_LED_PIN, 0);
            break;

        case (B_LED_PIN):
            nrf_gpio_pin_write(B_LED_PIN, 0);
            break;

        default:
            break;
    }
}