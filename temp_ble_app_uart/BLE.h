/**
***************************************************************
* @file BLE.h
* @author Kate McNamara 
* @date 2021-05-25
* @brief BLE peripheral driver header file
***************************************************************
*/

/* Includes -----------------------------------------------------------------*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* nRF Includes -------------------------------------------------------------*/
#include "app_error.h"
#include "app_util_platform.h"
#include "boards.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* BLE Includes -------------------------------------------------------------*/
#include "app_timer.h"
#include "app_uart.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_hci.h"
#include "ble_nus.h"
#include "bsp_btn_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"

#if defined (UART_PRESENT)
    #include "nrf_uart.h"
#endif

#if defined (UARTE_PRESENT)
    #include "nrf_uarte.h"
#endif

#ifndef BLE_H_
#define BLE_H_

/* Private Defines ----------------------------------------------------------*/
//<drowe> This chip needs to print to a hardware UART and the transfers that to the BLE NUS module. 
//<drowe> Select pins here that are not used for anything else.
#define RX_PIN      22 // (P0.22) 
#define TX_PIN      23 // (P0.23) 
#define RTS_PIN     24 // (P0.24) 
#define CTS_PIN     25 // (P0.25) 

/* Nordic Functions ---------------------------------------------------------*/
//static void nus_data_handler(ble_nus_evt_t * p_evt);
static void advertising_init(void);
static void advertising_start(void);
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context);
static void ble_stack_init(void);
static void buttons_leds_init(bool * p_erase_bonds);
static void conn_params_error_handler(uint32_t nrf_error);
static void conn_params_init(void);
static void gap_params_init(void);
static void log_init(void);
static void nrf_qwr_error_handler(uint32_t nrf_error);
static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt);
static void power_management_init(void);
static void services_init(void);
static void sleep_mode_enter(void);
static void timers_init(void);
static void uart_init(void);
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name);
void bsp_event_handler(bsp_event_t event);
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt);
void gatt_init(void);
void idle_state_handle(void);
void send_ble(float pData);
void uart_event_handle(app_uart_evt_t * p_event);

/* <kmcn> Functions ---------------------------------------------------------*/
void init_ble(void);
void execute_ble(void);

#endif