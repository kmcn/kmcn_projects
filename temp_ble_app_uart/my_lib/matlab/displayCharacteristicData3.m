function displayCharacteristicData3(src,evt)
    global bool_save;
    global fileID1;
    global init_plot1;
    global freq_sel;
    global p1;
    global t1;
    global x1;
    global y1;
    global ax1;
    [data,timestamp] = read(src,'oldest');
    
%     temp1 = double(typecast(uint8(data(1)/100), 'uint32'));
    temp2 = data(2);
    temp3 = data(3);
    temp4 = data(4);
    temp = double(typecast(uint8(data(1:4)), 'uint32'));

%     data_format = [string(temp1) ',' string(temp2) ',' string(temp3) ',' string(temp4)];
    data_format = [string(temp/1000)];
    str = join([string(timestamp,'hh:mm:ss') ',' data_format]);
    disp(str);
    % save the data in the fileID1 if boolean save is on
    if(bool_save ==1) % creates a file and saves data is 1
       fprintf(fileID1,'%s \n',str); 
    end
    
    % When is the right freqyuency, it will plot it
%     if(freq == freq_sel)
%         if(init_plot1==1) %first time initia;ises the plot and links the XData and YData source to the variables
%             figure('Name','WO Hydration 1'); ax1=axes;
%             t1 = timestamp;
%             x1(init_plot1) = seconds(timestamp -t1);
%             y1(init_plot1) = mag;
%             p1 = plot(ax1,x1,y1);
%             xlabel('Time (s)');
%             ylabel('Impedance Magnitude')
%             p1.XDataSource = 'x1';
%             p1.YDataSource = 'y1';
%             init_plot1 = init_plot1+1;
%         else
%             x1(init_plot1)=seconds(timestamp-t1);
%             y1 (init_plot1)=mag;
%             refreshdata;
%             ax1;
%             drawnow;
%             init_plot1 = init_plot1+1;
%         end
%             
%     end
    
end
