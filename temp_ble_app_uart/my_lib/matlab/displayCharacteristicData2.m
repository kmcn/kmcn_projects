function displayCharacteristicData2(src,evt)
    global bool_save;
    global fileID2;
    global init_plot2;
    global freq_sel;
    global p2;
    global t2;
    global x2;
    global y2;
    global ax2;
    
    [data,timestamp] = read(src,'oldest');
    freq = double(typecast(uint8(data(5:8)), 'uint32'));
    mag = double(typecast(uint8(data(1:4)), 'uint32'));
    phase = double(typecast(uint8(data(9:12)), 'uint32'));
    data_format = [string(freq) ',' string(mag) ',' string(phase)];
    str = join([string(timestamp,'hh:mm:ss') ',' data_format]);
    disp(str);
    
    if(bool_save ==1) % creates a file and saves data is 1
       fprintf(fileID2,'%s \n',str); 
    end
    % When is the right freqyuency, it will plot it
    if(freq == freq_sel)
        if(init_plot2==1) %first time initia;ises the plot and links the XData and YData source to the variables
            figure('Name','WO Hydration 2');ax2 = axes; 
            t2 = timestamp;
            x2(init_plot2) = seconds(timestamp -t2);
            y2(init_plot2) = mag;
            p2 = plot(ax2,x2,y2);
            xlabel('Time (s)');
            ylabel('Impedance Magnitude')
            p2.XDataSource = 'x2';
            p2.YDataSource = 'y2';
            init_plot2 = init_plot2+1;
        else
            x2(init_plot2)=seconds(timestamp-t2);
            y2 (init_plot2)=mag;
            refreshdata;
            ax2;
            drawnow;
            init_plot2 = init_plot2+1;
        end
            
    end
end