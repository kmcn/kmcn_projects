clear all

disp('Welcome to WO BLE app. Please follow the instructions')
global bool_save; % For saving data. This is called in displaydata function
global fileID1; %file ID for each device
global fileID2;
global init_plot1; % initialised plot
global init_plot2;
global freq_sel; %Selected frequency to be plotted
global p1;
global x1;
global y1;
global p2;
global x2;
global y2;
global ax1;
global ax2;


freq_sel = 1000;
init_plot1 = 1;
init_plot2=1;

bool_save = input('if you want to record the measurement enter 1 if not enter 0 \n');
if (bool_save == 1) % info for saving in the file
    str_ON = input('\nOperator Name: \n','s');
    str_PN = input('\nPatient Name: \n','s');
    str_L = input('\nLocation of devices: \n','s');
    str_D = input('\nDetailed experiment description: \n','s');
end

disp('Looking for BLE devices. Please wait ...');
devlist = blelist("Name","WO", 'timeout', 10);
[r,~]=size(devlist);
num_dev = input('\n Please insert the number of devices to be connected: \n');

while(r<num_dev) % if number of devices is bigger than the availables then keep asking
    disp('Number of devices exceeds number of devices available');
    if (r<num_dev)
        num_dev = input('\nPlease insert the number of devices to be connected: \n');
    end
end

%% Connects to devices
% Device 1
d = datetime(now,'ConvertFrom','datenum');
new_d = regexprep(datestr(d),':','');
% root_name_file = join([new_d '_' str_D]);
if(~isempty(find(devlist.Name == "WO Hydration 1")))
    if (bool_save ==1) % creates a file and saves data is 1
        file1 = join([new_d '_' str_D '_WO Hydration 1.txt']);
        fileID1 = fopen(file1,'at');
        fprintf(fileID1,'Date and Time: %s \n',d);
        fprintf(fileID1,'Opeator Name: %s \n',str_ON); 
        fprintf(fileID1,'Patient Name: %s \n',str_PN); 
        fprintf(fileID1,'Number of devices:: %.0f \n',num_dev);
        fprintf(fileID1,'Location of deices: %s \n',str_L);
        fprintf(fileID1,'Detailed experiment description: %s \n\n',str_D);
        fprintf(fileID1,'WO Hydration 1\n');
        fprintf(fileID1,'Time, Frequency, Magnitude, -Phase \n');
    end
    b1 = ble(devlist.Name(find(devlist.Name == "WO Hydration 1")));
    c1 = characteristic(b1,...
        b1.Characteristics.ServiceUUID(6),...
        b1.Characteristics.CharacteristicUUID(6));
    c1.DataAvailableFcn = @displayCharacteristicData1;
      
end
% Device 2
if(~isempty(find(devlist.Name == "WO Hydration 2")))
    if (bool_save ==1)
        file2 = join([new_d '_' str_D '_WO Hydration 2.txt']);
        fileID2 = fopen(file2,'at');
        fprintf(fileID2,'Date and Time: %s \n',d);
        fprintf(fileID2,'Opeator Name: %s \n',str_ON); 
        fprintf(fileID2,'Patient Name: %s \n',str_PN); 
        fprintf(fileID2,'Number of devices:: %.0f \n',num_dev);
        fprintf(fileID2,'Location of deices: %s \n',str_L);
        fprintf(fileID2,'Detailed experiment description: %s \n\n',str_D);
        fprintf(fileID2,'WO Hydration 2\n');
        fprintf(fileID2,'Time, Frequency, Magnitude, -Phase \n');
    end
    
    b2 = ble(devlist.Name(find(devlist.Name == "WO Hydration 2")));
    c2 = characteristic(b2,...
        b2.Characteristics.ServiceUUID(6),...
        b2.Characteristics.CharacteristicUUID(6));
    c2.DataAvailableFcn = @displayCharacteristicData2; 
end
% Device 3
if(~isempty(find(devlist.Name == "WO Hydration 3")))
    b3 = ble(devlist.Name(find(devlist.Name == "WO Hydration 3")))
    c3 = characteristic(b3,...
        b3.Characteristics.ServiceUUID(6),...
        b3.Characteristics.CharacteristicUUID(6))
    c3.DataAvailableFcn = @displayCharacteristicData1
end
% Device 4
if(~isempty(find(devlist.Name == "WO Hydration 4")))
    b4 = ble(devlist.Name(find(devlist.Name == "WO Hydration 4")))
    c4 = characteristic(b4,...
        b4.Characteristics.ServiceUUID(6),...
        b4.Characteristics.CharacteristicUUID(6))
    c4.DataAvailableFcn = @displayCharacteristicData
end
% Device 5
if(~isempty(find(devlist.Name == "WO Temperature 1")))
    b5 = ble(devlist.Name(find(devlist.Name == "WO Temperature 1")))
    c5 = characteristic(b5,...
        b5.Characteristics.ServiceUUID(6),...
        b5.Characteristics.CharacteristicUUID(6))
    c5.DataAvailableFcn = @displayCharacteristicData3
end



unsubscribe(c1)
c1.DataAvailableFcn = [];
unsubscribe(c2)
c2.DataAvailableFcn = [];

fclose(fileID1)
fclose(fileID2)
fclose(3)
fclose(4)
fclose(5)
clear all

% [datan1 stamp1]=read(c1)

x = seconds(stamp1-stamp)

ylim(gca,[0 2000])