function displayCharacteristicData1(src,evt)
    global bool_save;
    global fileID1;
    global init_plot1;
    global freq_sel;
    global p1;
    global t1;
    global x1;
    global y1;
    global ax1;
    [data,timestamp] = read(src,'oldest');
    
    freq = double(typecast(uint8(data(5:8)), 'uint32'));
    mag = double(typecast(uint8(data(1:4)), 'uint32'));
    phase = double(typecast(uint8(data(9:12)), 'uint32'));
    device = double(typecast(uint8(data(13)), 'uint8'));
    data_format = [string(freq) ',' string(mag) ',' string(phase) ',' string(device)];
    
    str = join([string(timestamp,'hh:mm:ss') ',' data_format]);
    disp(str);
    % save the data in the fileID1 if boolean save is on
    if(bool_save ==1) % creates a file and saves data is 1
       fprintf(fileID1,'%s \n',str); 
    end
    
    % When is the right freqyuency, it will plot it
%     if(freq == freq_sel)
%         if(init_plot1==1) %first time initia;ises the plot and links the XData and YData source to the variables
%             figure('Name','WO Hydration 1'); ax1=axes;
%             t1 = timestamp;
%             x1(init_plot1) = seconds(timestamp -t1);
%             y1(init_plot1) = mag;
%             p1 = plot(ax1,x1,y1);
%             xlabel('Time (s)');
%             ylabel('Impedance Magnitude')
%             p1.XDataSource = 'x1';
%             p1.YDataSource = 'y1';
%             init_plot1 = init_plot1+1;
%         else
%             x1(init_plot1)=seconds(timestamp-t1);
%             y1 (init_plot1)=mag;
%             refreshdata;
%             ax1;
%             drawnow;
%             init_plot1 = init_plot1+1;
%         end
%             
%     end
    
end