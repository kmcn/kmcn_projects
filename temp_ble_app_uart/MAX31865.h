/**
***************************************************************
* @file   MAX31865.h
* @author Kate McNamara 
* @date   2021-04-15
* @brief  MAX31865 temperature sensor peripheral driver for nRF52840 - header file
* @about  Adapted from https://github.com/nimaltd/max31865 and https://github.com/adafruit/Adafruit_MAX31865, 
          licensed under GNU General Public License V3.0
***************************************************************
*/

/* Includes -----------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "app_error.h"
#include "app_util_platform.h"
#include "boards.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#ifndef MAX31865_H_
#define MAX31865_H_

/* Private Defines ----------------------------------------------------------*/
#define MAX31865_CONFIG_REG       0x00
#define MAX31865_CONFIG_BIAS      0x80
#define MAX31865_CONFIG_MODEAUTO  0x40
#define MAX31865_CONFIG_MODEOFF   0x00
#define MAX31865_CONFIG_1SHOT     0x20
#define MAX31865_CONFIG_3WIRE     0x10
#define MAX31865_CONFIG_24WIRE    0x00
#define MAX31865_CONFIG_FAULTSTAT 0x02
#define MAX31865_CONFIG_FILT50HZ  0x01
#define MAX31865_CONFIG_FILT60HZ  0x00

#define MAX31865_RTDMSB_REG       0x01
#define MAX31865_RTDLSB_REG       0x02
#define MAX31865_HFAULTMSB_REG    0x03
#define MAX31865_HFAULTLSB_REG    0x04
#define MAX31865_LFAULTMSB_REG    0x05
#define MAX31865_LFAULTLSB_REG    0x06
#define MAX31865_FAULTSTAT_REG    0x07

#define MAX31865_FAULT_HIGHTHRESH 0x80
#define MAX31865_FAULT_LOWTHRESH  0x40
#define MAX31865_FAULT_REFINLOW   0x20
#define MAX31865_FAULT_REFINHIGH  0x10
#define MAX31865_FAULT_RTDINLOW   0x08
#define MAX31865_FAULT_OVUV       0x04

#define RTD_A                     3.9083e-3
#define RTD_B                     -5.775e-7

#define _MAX31865_RNOMINAL        100.0f
#define _MAX31865_RREF            430.0f

/* Typedefs  ----------------------------------------------------------------*/
typedef struct {
	uint16_t cs_pin;
	// nrf_drv_spi_t spi;   
	uint8_t lock;
} Max31865;

/* Internally Called Functions ----------------------------------------------*/
uint8_t read_fault(Max31865 *max31865);
uint8_t read_register_8(Max31865 *max31865, uint8_t addr);
uint16_t read_register_16(Max31865 *max31865, uint8_t addr);
uint16_t read_rtd(Max31865 *max31865);
void auto_convert(Max31865 *max31865, uint8_t enable);
void clear_fault(Max31865 *max31865);
void enable_bias(Max31865 *max31865, uint8_t enable);
void read_register_n(Max31865 *max31865, uint8_t addr, uint8_t *buffer, uint8_t n);
void set_filter(Max31865 *max31865, uint8_t filterHz);
void set_wires(Max31865 *max31865, uint8_t numWires);
void write_register_8(Max31865 *max31865, uint8_t addr, uint8_t data);

/* Externally Called Functions ----------------------------------------------*/
bool read_temperature(Max31865 *max31865, float *readTemp);
bool read_temperature_fahrenheit(Max31865 *max31865, float *readTemp);
float filter_temperature(float newInput, float	lastOutput, float efectiveFactor);
void max318565_init(Max31865 *max31865, uint8_t numWires, uint8_t filterHz);
// void spi_event_handler(nrf_drv_spi_evt_t const *p_event, void *p_context);

#endif